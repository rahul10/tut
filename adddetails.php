<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=1024, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />
<title>Lonari Info</title> 
<link rel="stylesheet" href="http://localhost/lonari_Info/assets/css/lonariinfo.css" type="text/css">
<link type="text/css" rel="stylesheet" href="css/pagination.css" />
<link type="text/css" rel="stylesheet" href="css/paginationB.css" />
</head>
<body>
<div id="hd">
	<div class="wdt980 clearfix" align="center"><img src="http://localhost/lonari_Info/assets/images/logo.png" width="125" height="106" border="0" alt=""></div>
</div>
<div class="spacer5">&nbsp;</div>
<div id="menu">			
	<div class="menuctnt wdthmenu">
		<ul class="clearfix wdt980" >
			<li><a href="search.html">Search</a></li>
			<li><a href="javascript:void(0);">Add Details</a></li>			
			<li class="last"><a href="/lonari_Info/index.php/verifylogin/logout">logout</a></li>			
		</ul>
	</div>			
</div>
<div class="spacer25">&nbsp;</div>
<div class="wdt980 clearfix" align="center">
		<?php echo $error; ?>
		<form enctype="multipart/form-data" method="post" action="http://localhost/lonari_Info/index.php/addcontroller/insert_data" >
		<div class="table" style="padding:25px;">
			<h2 class="secttl">Add Details  &raquo;</h2>
			<div  align="left">
			<table cellspacing="0" cellpadding="0" border="0" class="dettab">
				<tr>
					<td width="100"><b>Title:</b></td>
					<td width="450">
						<select name="title">
							<option value="" selected>Title</option>
							<option value="mr" >Mr.</option>
							<option value="mrs" >Mrs.</option>
							<option value="miss" >Miss</option>
							<option value="dr" >Dr.</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Name:</b></td>
					<td><input type="text" name="user" class="input" maxlength="30"></td>
				</tr>
				<tr>
					<td><b>Date Of Birth:</b></td>
					<td>
						<select name="day">
							<option value="" selected>Day</option>
							<option value="1" >1</option>
							<option value="2" >2</option>
							<option value="3" >3</option>
							<option value="4" >4</option>
							<option value="5" >5</option>
							<option value="6" >6</option>
							<option value="7" >7</option>
							<option value="8" >8</option>
							<option value="9" >9</option>
							<option value="10" >10</option>
							
						</select>
						<select name="month">
							<option value="" selected>Month</option>
							<option value="jan" >January</option>
							<option value="feb" >february</option>
							<option value="march" >March</option>
							<option value="april" >April</option>
							<option value="may" >May</option>
							<option value="june" >June</option>
							<option value="july" >July</option>
							<option value="aug" >August</option>
							<option value="sept" >September</option>
							<option value="oct" >October</option>
							<option value="nov" >November</option>
							<option value="dec" >December</option>
							
						</select>
						<select name="year">
							<option value="" selected>Year</option>
							<option value="2011" >2011</option>
							<option value="2012" >2012</option>
							<option value="2013" >2013</option>
							<option value="2014" >2014</option>
							<option value="2015" >2015</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><b>Age:</b></td>
					<td><input type="text" name="age" class="input" ></td>
				</tr>
				<tr>
					<td><b>Status:</b></td>
					<td><input type="text" name="status" class="input" ></td>
				</tr>
				<tr>
					<td><b>Upload Photo:</b></td>
					<td><input type="file" name="userfile" ></td>
				</tr>
				<tr>
					<td valign="top"><b>Address:</b></td>
					<td><textarea name="address" class="textarea" ></textarea></td>
				</tr>
				<tr>
					<td><b>City / Taluka:</b></td>
					<td><input type="text" name="city" class="input" ></td>
				</tr>
				<tr>
					<td><b>District:</b></td>
					<td><input type="text" name="district" class="input" ></td>
				</tr>
				<tr>
					<td><b>State:</b></td>
					<td><input type="text" name="state" class="input" ></td>
				</tr>
				<tr>
					<td><b>Pincode:</b></td>
					<td><input type="text" name="pin" class="input"></td>
				</tr>
				<tr>
					<td valign="top"><b>Native Place:</b></td>
					<td><textarea name="nplace" class="textarea" ></textarea></td>
				</tr>
				<tr>
					<td><b>Phone / Mobile:</b></td>
					<td><input type="text" name="phone" class="input"></td>
				</tr>
				<tr>
					<td><b>Email:</b></td>
					<td><input type="text" name="email" class="input" ></td>
				</tr>
				<tr>
					<td><b>Education:</b></td>
					<td><input type="text" name="education" class="input" ></td>
				</tr>
				<tr>
					<td><b>Occupation:</b></td>
					<td><input type="text" name="occupation" class="input" ></td>
				</tr>
				<tr>
					<td><b>Office Address:</b></td>
					<td><input type="text" name="offaddress" class="input" ></td>
				</tr>
			</table></div>	<br><br>	
			<h2 align="left">Other Family Member Details</h2><br>
			<table cellspacing="0" cellpadding="0" border="0" width="100%"  class="table tbbg">
				<tr>
					<td>
						<table cellspacing="1" cellpadding="10" border="0" class="innertable" style="font-size:12px">
							<tr>
								<td width="50px" align="center"><b>Sr. No.</b></td>
								<td width="325px" align="center"><b>Name</b></td>
								<td width="150px" align="center"><b>Relation</b></td>
								<td width="150px" align="center"><b>Age</b></td>
								<td width="150px" align="center"><b>Education</b></td>
								<td width="150px" align="center"><b>Occupation</b></td>
								<td width="150px" align="center"><b>Contact Number</b></td>
							</tr>
							<tr>
								<td align="center">1</td>
								<td><input type="text" name="memname[]" class="input"></td>
								<td align="center">
									<select name="memrelation[]">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="memage[]" class="input inputsml"></td>
								<td align="center"><input type="text" name="memeducation[]" class="input inputmed"></td>
								<td align="center">
									<select name="memoccupation[]">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="memcontact[]" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">2</td>
								<td><input type="text" name="memname[]" class="input"></td>
								<td align="center">
									<select name="memrelation[]">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="memage[]" class="input inputsml"></td>
								<td align="center"><input type="text" name="memeducation[]" class="input inputmed"></td>
								<td align="center">
									<select name="memoccupation[]">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="memcontact[]" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">3</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">4</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">5</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione[]">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>							</tr>
							<tr>
								<td align="center">6</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">7</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">8</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">9</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
							<tr>
								<td align="center">10</td>
								<td><input type="text" name="rname" class="input"></td>
								<td align="center">
									<select name="relation">
										<option value="" selected>Relation</option>
										<option value="husband" >Husband</option>
										<option value="wife" >Wife</option>
										<option value="son" >Son</option>
										<option value="daughter" >Daughter</option>
										<option value="uncle" >Uncle</option>
										<option value="aunty" >Aunty</option>
										<option value="son_in_law" >Son-in-law</option>
										<option value="daughter_in_law" >Daughter-in-law</option>
										<option value="grandson" >Grand Son</option>
										<option value="granddaughter" >Grand Daughter</option>
										<option value="garndfather" >Grand Father</option>
										<option value="grandmother" >Grand Mother</option>
									</select>
								</td>
								<td align="center"><input type="text" name="agee" class="input inputsml"></td>
								<td align="center"><input type="text" name="educatione" class="input inputmed"></td>
								<td align="center">
									<select name="occupatione">
										<option value="" selected>Occupation</option>
										<option value="service">Service</option>
										<option value="business" >Business</option>
										<option value="farmer" >Farmer</option>
										<option value="tecnician" >Technician</option>
										<option value="engineer" >Engineer</option>
										<option value="advocate" >Adovate</option>
										<option value="doctor" >Doctor</option>
										<option value="student" >Student</option>
									</select>
								</td>
								<td align="center"><input type="text" name="contacte" class="input inputmed"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br><br>
			<h2 align="left">Other Information</h2><br>
			<div align="left"><textarea name="otherinfo" class="textarea" ></textarea></div>
			<br><br><input type="submit" value="Submit" class="submit">
		</div>
	</form>
</div><br><br><br><br>
</body>
</html>
