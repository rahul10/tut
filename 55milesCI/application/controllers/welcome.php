<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct() {
		parent::__construct();
		$this->load->model('insert_model');
}
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('main_view');
		$this->load->view('common/footer');
		
		$data=array(
		'position' => $this->input->post('position'),
		'name' => $this->input->post('name'),
		'email' => $this->input->post('email'),    
		'phone' =>$this->input->post('phone')	
		);
		$this->insert_model->form_insert($data);
		
		// Loading View
		//$this->load->view('insert_view');
	}
	
	
	/*private $data;
/
public function index() {
    if ($this->input->is_ajax_request()){
        $result = $this->_subscribe();
        echo json_encode($result);
        exit;
    }
    $this->load->view('soon', $this->data);
}

private function _subscribe(){
    $success = true;
    if(!$this->input->get('firstname')){
        $message =  "No firstname provided";
        $success = false;
    }
    if($success && !$this->input->get('lastname')){
        $message =  "No lastname provided";
        $success = false;
    }
    if($success && !$this->input->get('emailaddress')){
        $message =  "No email address provided";
        $success = false;
    }
    if ($success){
        $this->load->library('Mcapi', array(
            'apikey' => $this->config->item('mailchimp_key')
        ));
        if ($this->mcapi->listSubscribe($this->config->item('mailchimp_list_id'), $this->input->get('emailaddress'), array(
            'FNAME' => $this->input->get('firstname'),
            'LNAME' => $this->input->get('lastname')
        )) === true){
            //  It worked!
            $message = 'Success! Check your email to confirm sign up.';
        } else {
            $success = false;
            //  An error ocurred, return error message
            $message =  'Error: ' . $this->mcapi->errorMessage;
        }
    }
    return array('message' => $message, 'success' => $success);
}*/
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>