<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct() {
		parent::__construct();
		$this->load->model('insert_model');
}
	public function index()
	{
		$this->load->view('home');
		}
	public function addUser()
	{
		$this->load->view('common/header');
		$this->load->view('main_view');
		$this->load->view('common/footer');
		
		
	}
	public function joinUs()
		{
			$data=array(
		'position' => $this->input->post('position'),
		'name' => $this->input->post('name'),
		'email' => $this->input->post('email'),    
		'phone' =>$this->input->post('phone'),
		'photo' =>$_FILES['userfile']['name'] 
		//'file'  => $data1
		);
		print_r($data);
		//-----------------------------------------------------
		//upload image in folder.
		
		$config['upload_path']= './uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp';
		$config['max_size']	= '20000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768'; 
		
//load the library upload to upload the image in the folder.

		$this->load->library('upload', $config);
		
		if(!$this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			//$this->load->view('main_view', $error);
		}
		else
		{
			
			$data1 = array('upload_data' => $this->upload->data());
			

		}
		
		//$this->insert_model->form_insert($data);
		//$Arr	=	$data('position' => '','name' => '','email' => '','phone' => '','userfile' =>'');
			if($data=='')
				{
					echo "Please Enter Values";
				}
			else
				{
					$this->insert_model->form_insert($data);
					echo "<script>alert('Success');</script>";
					$this->load->view('common/header');
					$this->load->view('main_view');
					$this->load->view('common/footer');
				}	
		}

	public function about()
		{
			$this->load->view('common/header');
			$this->load->view('about');
			$this->load->view('common/footer');
		}
	public function screens()
		{
			$this->load->view('common/header');
			$this->load->view('screens');
			$this->load->view('common/footer');
		}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>