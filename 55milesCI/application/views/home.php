<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>55MILES</title>

<link href="assets/css/reset.css" rel="stylesheet" type="text/css" />

<link href="assets/css/main.css" rel="stylesheet" type="text/css" />

<link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="assets/css/flexslider.css" type="text/css" media="screen" />

<script src="assets/scripts/jquery.min.js" type="text/javascript"></script>

<script defer src="assets/scripts/jquery.flexslider.js"></script>

<script type="text/javascript">

$(function(){

SyntaxHighlighter.all();

});

$(window).load(function(){

$('.flexslider').flexslider({

animation: "slide",

start: function(slider){

$('body').removeClass('loading');

}

});

});

</script>

</head>



<body>

<div class="landingBannerBackground">

  <div class="container">

    <div class="header">

      		<a href="#"><div class="miles"><h2>55Miles</h2></div></a>

      <div class="headerRight">

        <div class="headerLogin">

          <a href="http://stage.55miles.com/content/login/register"><input name="login" type="button" class="loginButton" value="Sign up" /></a>

          <a href="http://stage.55miles.com"><input name="login" type="button" class="loginButton" value="login" /></a>

        </div>

        <div class="socialMedia"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-linkedin-square"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> </div>

      </div>

    </div>

   

   <div class="banner">

      <div class="image">

      <div class="flexslider">

       <ul class="slides">

          <li><img src="assets/images/tab1.jpg" alt="" /></li>

        

        </ul>

        </div>

      </div>

      

      <div class="text">

              <h2>55Miles</h2>

              <h3>Management and collaboration at work place</h3>

              <a href="<?php echo base_url("home/addUser");?>"><input name="Banner Button" type="button" class="bannerButton" value="To join us" /></a>

              <a href="<?php echo base_url("home/about");?>"><input name="Banner Button" type="button" class="bannerButton" value="Why 55 Miles" /></a>

 </div>

    </div>

    

    <div class="clearFix"></div>

  </div>

  <div class="bannerOverlay">

    <a href="screens.html"><div class="clickHere"><img src="assets/images/clickhere.png" alt="" /></div></a>

  </div>

</div>

<div class="askYourself">

  <div class="container">

    <h1>ASK yourself</h1>

    <div class="askYourselfBlock">

      <div class="image"><img src="assets/images/askyourselfimg1.png" alt="" /></div>

      <div class="text">

        <p>Is fire-fighting, last minute rush and crisis like emergency a common phenomenon at your workplace?</p>

      </div>

    </div>

    <div class="askYourselfBlock borderOff">

    <div class="imageTwo"><img src="assets/images/askyourselfimg2.png" alt="" /></div>

      <div class="textTwo">

        <p>Do worries of work place, meeting deadlines, handling problems, customer billing and cash receipts, sub-ordinates working on expected lines have ever disturbed your sleep in the night?</p>

      </div>

      

    </div>

    <div class="clearFix"></div>

  </div>

</div>

<div class="newsletterBackground">

  <div class="container">

    <div class="newsletterBox">

      <h2>If the answer to either of the above is - YES, then 55 Miles is definitely for you.</h2>

      <h3>For updates on 55 Miles, mention your email-ID here</h3>

      <div class="newsletterBoxInputGroup">

        <input type="text" class="newsletterBoxInput" value="Enter Email ID" name="Email" id="Email"

onblur="if (this.value == '') {this.value = 'Enter Email ID';}"

onfocus="if (this.value == 'Enter Email ID') {this.value = '';}" />

        <input type="button" value="SUBMIT" class="newsletterBoxButton" />

      </div>

    </div>

    <div class="clearFix"></div>

  </div>

</div>

<div class="schematicBlock">

  <div class="container">

    <div class="image"><img src="assets/images/images01.jpg" alt="" /></div>

    <a href="#" class="schematicButton">For Schematic presentation</a>

    <div class="clearFix"></div>

  </div>

</div>

<div class="footer">

  <div class="container">

    <p>© 2014 55mIes. All rights reserved </p>

    <div class="footerSocialMedia"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-linkedin-square"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> </div>

    <div class="clearFix"></div>

  </div>

</div>

</body>

</html>

