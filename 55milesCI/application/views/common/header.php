<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>55MILES | Contact</title>

<link href="<?php echo SITE_URL; ?>/assets/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_URL; ?>/assets/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="<?php echo SITE_URL; ?>/assets/css/main.css" rel="stylesheet" type="text/css" />

<link href="<?php echo SITE_URL; ?>/assets/css/font-awesome.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/flexslider.css" type="text/css" media="screen" />


<script src="<?php echo SITE_URL; ?>/assets/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script defer src="<?php echo SITE_URL; ?>/assets/scripts/jquery.flexslider.js"></script>
<script src="<?php echo SITE_URL; ?>/assets/scripts/jquery.bxslider.min.js" type="text/javascript"></script>



<script type="text/javascript">
	 $(document).ready(function () {           
		 $('.bxslider').bxSlider({
			 mode: 'horizontal',
			 slideMargin: 3,
			 auto:true
		 });             
	 });
</script>

</head>



<body>

<div class="landingBannerBackground">

  <div class="container">

    <div class="header">
		<div class="headerleft">
			<div class="banner-sm">

				<div class="text">

				  <h2>55Miles</h2>

				  <h3>Management and collaboration at work place</h3>

				   <a href="<?php echo base_url('home/screens');?>"><input name="Banner Button" type="button" class="bannerButton-gray" value="Snapshots" /></a>

				   <a href="<?php echo base_url('home/about');?>"><input name="Banner Button" type="button" class="bannerButton-gray" value="Why 55 Miles" /></a>
				  <a href="<?php echo base_url('home/addUser');?>"><input name="Banner Button" type="button" class="bannerButton-gray" value="To Join Us" /></a>

				</div>

			</div>
		</div>

      <div class="headerRight">

        <div class="headerLogin">

          
		  		  <a href="http://stage.55miles.com/content/login/register"> <input name="login" type="button" class="loginButton" value="Sign up" /></a>

         <a href="http://stage.55miles.com/"> <input name="login" type="button" class="loginButton" value="login" /></a>

        </div>

        <div class="socialMedia"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-linkedin-square"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> </div>

      </div>

    </div>

   

   <div class="banner-sm">

		

    </div>

    

    <div class="clearFix"></div>

  </div>